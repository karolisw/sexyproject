import java.io.*;

import java.util.*;
;

public class SuperClass {
     Node startNode;
     Node endNode;

    //not correct to have this list if a landmark isn't a node
    ArrayList<Node> landMarks = new ArrayList<>(5); //does not need space for more than 5 nodes
    int infinity = Integer.MAX_VALUE;
    // Can be read from file
    int numberOfVertices;
    int numberOfEdges;
    Node [] nodes; // = new Node[numberOfVertices];
    Node [] invertedNodes;
    //int [][] preProcessedLandmarks; //= new int[numberOfVertices][5];
    int [][] preProcessingToLandmarks;
    int [][] preProcessingFromLandmarks;

    // This map keeps track of the possible indices a particular
    // node value is found in the heap. Having this mapping lets
    // us have O(log(n)) removals and O(1) element containment check
    // at the cost of some additional space and minor overhead
    private Map<Node, TreeSet<Integer>> map = new HashMap<>();

    PriorityQueue<Node> priorityQueue = new PriorityQueue<>();

    String[] field = new String[10]; //Maximum 10 fields in a line (BufferedReader)



    /**
     * A node is:
     * Reached = when distance != infinity
     * Settled = when settled == true
     * First reached, then settled
     * Taken out of PQ when settled, not reached
     *
     * A node has 3 distance-attributes:
             * Distance to start (not static and might change)
             * Distance to goal (getDistanceBetweenNodes) is calculated when node is added to PQ
             * Sum of the two first distances (used to compare priority by comparator when node is added to PQ)
     */
    public class Node{
        Node parent;
        String name;
        ArrayList<EdgeToNeighbor> edgesToNeighbors;
        int nodeCode; //ex. 2 = gas station
        int nodeNr; //id
        int distanceToStart;
        int distanceToGoal;
        int summedDistance;
        boolean reached;
        boolean settled;
        double latitude;  // --------
        double longitude; // ||||||

        /**
         * Distance to node = infinite at initialization
         * Settled = true when node is taken out of PQ
         * SuperClass.Dijkstra's algorithm only uses one distance (distanceToStart)
         *
         * @param nodeNr is the node id
         */
        public Node(int nodeNr){ //100 (from)
            parent = null;
            name = "";
            edgesToNeighbors = new ArrayList<>(5); //11, 1000 (to)
            this.nodeCode = 0; // Can be found using the file containing node codes
            this.nodeNr = nodeNr;
            distanceToGoal = infinity;
            distanceToStart = infinity;
            summedDistance = 0;
            reached = false;
            settled = false;
            latitude = 0;
            longitude = 0;
        }

        public Node(Node node){
            this.parent = null;
            this.nodeNr = node.nodeNr;
            this.nodeCode = node.nodeCode;
            edgesToNeighbors = new ArrayList<>(5);
            this.name = node.name;
            this.distanceToGoal = node.distanceToGoal;
            this.distanceToStart = node.distanceToStart;
            this.summedDistance = node.summedDistance;
            this.reached = node.reached;
            this.settled = node.settled;
            latitude = node.latitude;
            longitude = node.longitude;
        }


        public void setName(String name) {
            this.name = name;
        }

        public void setNodeCode(int nodeCode) {
            this.nodeCode = nodeCode;
        }

        /**
         * Cannot bring parent-node into the print due to it being null until reached
         * @return
         */
        @Override
        public String toString() {
            return "Node " + nodeNr + ": " +
                    "\nName = '" + name + '\'' +
                    "\nnodeCode = " + nodeCode +
                    "\ndistanceToStart = " + distanceToStart +
                    "\ndistanceToGoal = " + distanceToGoal +
                    "\nsummedDistance = " + summedDistance +
                    "\nreached = " + reached +
                    "\nsettled = " + settled +
                    "\nlatitude = " + latitude +
                    "\nlongitude = " + longitude +
                    "\nedgesToNeighbors = " + edgesToNeighbors +
                    '\n';
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (!(o instanceof Node)) return false;

            Node node = (Node) o;

            if (nodeCode != node.nodeCode) return false;
            if (nodeNr != node.nodeNr) return false;
            if (distanceToStart != node.distanceToStart) return false;
            if (distanceToGoal != node.distanceToGoal) return false;
            if (summedDistance != node.summedDistance) return false;
            if (reached != node.reached) return false;
            if (settled != node.settled) return false;
            if (Double.compare(node.latitude, latitude) != 0) return false;
            if (Double.compare(node.longitude, longitude) != 0) return false;
            if (parent != null ? !parent.equals(node.parent) : node.parent != null) return false;
            if (name != null ? !name.equals(node.name) : node.name != null) return false;
            return edgesToNeighbors != null ? edgesToNeighbors.equals(node.edgesToNeighbors) : node.edgesToNeighbors == null;
        }

        @Override
        public int hashCode() {
            int result;
            long temp;
            result = parent != null ? parent.hashCode() : 0;
            result = 31 * result + (name != null ? name.hashCode() : 0);
            result = 31 * result + (edgesToNeighbors != null ? edgesToNeighbors.hashCode() : 0);
            result = 31 * result + nodeCode;
            result = 31 * result + nodeNr;
            result = 31 * result + distanceToStart;
            result = 31 * result + distanceToGoal;
            result = 31 * result + summedDistance;
            result = 31 * result + (reached ? 1 : 0);
            result = 31 * result + (settled ? 1 : 0);
            temp = Double.doubleToLongBits(latitude);
            result = 31 * result + (int) (temp ^ (temp >>> 32));
            temp = Double.doubleToLongBits(longitude);
            result = 31 * result + (int) (temp ^ (temp >>> 32));
            return result;
        }
    }

    /**
     * Object holds all information necessary for an edge to hold
     * This is not a neighbor, but an edge from the node to the neighboring node
     *
     */
    public class EdgeToNeighbor {
        int nodeNr; //the neighbor
        int time; //the weight

        public EdgeToNeighbor(int nodeNr, int time){
            this.nodeNr = nodeNr;
            this.time = time;
        }

        @Override
        public String toString() {
            return "Edge where " +
                    "nodeNr = " + nodeNr +
                    " and time = " + time +
                    '\n';
        }


    }


    /**
     * Set end node using Node object
     */
    public void setEndNode(Node endNode) {
        this.endNode = endNode;
    }

    /**
     * Method makes sure all nodes are un-reached
     * @param graph
     */
    public void notReached(Node[] graph){
        for (int i = 0; i < graph.length; i++) {
            graph[i].reached = false;
            graph[i].settled = false;
        }
    }




    /**
     * Set start node using Node object
     * the start node has distance = 0;
     */
    public void setStartNode(Node[] graph, Node startNode) {
        graph[startNode.nodeNr].distanceToStart = 0;

    }



    /**
     * This method will fill up attribute []field
     * For use as split() substitute when reading lines in from file using BufferedReader
     * Do not use method before handling of last result is done
     *
     * @param line is the current line
     * @param size
     */
    void newSplit(String line, int size) {
        int j = 0;
        int length = line.length();
        for (int i = 0; i < size; ++i) {

            //skipping all spaces
            while (line.charAt(j) <= ' ') ++j;
            int startOfWord = j;

            //skipping all non-spaces (finding the end of the word)
            while (j < length && line.charAt(j) > ' ') ++j;
            field[i] = line.substring(startOfWord, j);
        }
    }

    /**
     * Uses BufferedReader to read node-map of northern countries
     * Method also sets the number of vertices (class attribute)
     *
     * File format: nodeNr latitude longitude
     */
    public void getNodeAndLocationFromFile(File file) {
        BufferedReader bufferedReader = null;
        try {
            bufferedReader = new BufferedReader(new InputStreamReader(new FileInputStream(file)));
            String currentLine = bufferedReader.readLine();
            currentLine = currentLine.replace(" ", "");

            // The first line holds the number of vertices in the graph
            numberOfVertices = Integer.parseInt(currentLine);

            // The arrays are instantiated inside here to avoid null pointers
            nodes = new Node[numberOfVertices];

            preProcessingFromLandmarks = new int[numberOfVertices][5];
            preProcessingToLandmarks = new int[numberOfVertices][5];


            // Printing out the number of vertices
            System.out.println("number of vertices: " + numberOfVertices);

            // Reading the file
            while ((currentLine = bufferedReader.readLine()) != null) {

                //splitting the line and assigning all elements from split to "field"-attribute
                newSplit(currentLine, 3);

                //now, we finish our operations on the line information before moving on to next line
                Node node = new Node(Integer.parseInt(field[0]));
                node.latitude = Double.parseDouble(field[1]);
                node.longitude = Double.parseDouble(field[2]);

                //all nodes should have a unique node nr (edges will be added in readWeightsFromFile method)
                nodes[node.nodeNr] = node;

            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (bufferedReader != null) {
                    bufferedReader.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void copyNodesListOntoInvertedNodes(int numberOfVertices, Node[] nodes) {

        invertedNodes = new Node[numberOfVertices];
        invertedNodes = nodes.clone();
        /**
        int objects = 0;
        int total = 0;
        while (true){
            if (total == numberOfVertices){
                System.out.println("finished copying");
                break;
            }
            for (int i = 0; i < nodes.length; i++) {
                if(total == numberOfVertices){
                    System.out.println("inverted nodes is finished");
                    break;
                }
                invertedNodes[i] = new Node(nodes[i]);
                total ++;
                objects++;


                if (objects == 100000){
                    //else, we must break, but still stay inside of the while loop
                    System.out.println("reached 100 000 copies");
                    objects = 0;
                    break;
                }

            }
        }
        return invertedNodes;
         */

    }



    /**
     * Same principal as the method above
     * Use HSplit to split up the words
     * File format: fromNode toNode time length speedLimit
     * Time is the weigh, meaning that we do not need length and speedLimit
     * ToNode is the neighbor of FromNode
     *
     * //@param file is the file containing the weights
     */
    public void getWeightsFromFile(File file){
        BufferedReader bufferedReader = null;
        try{
            bufferedReader = new BufferedReader(new InputStreamReader(new FileInputStream(file)));

           // bufferedReader = new BufferedReader(new FileReader(file));
            String currentLine = bufferedReader.readLine();
            currentLine = currentLine.replace(" ", "");
            int lines = 0;

            numberOfEdges = Integer.parseInt(currentLine); //this should be the first line
            System.out.println("number of edges: " + numberOfEdges);

            while ((currentLine = bufferedReader.readLine()) != null) {
                if(lines == 5000){
                    System.gc();
                }
                splitAndAssign(currentLine);
                currentLine = null;
                lines ++;
            }

        }catch (IOException e){
            e.printStackTrace();
        } finally {
            try{
                if (bufferedReader != null){
                    bufferedReader.close();
                }
            }catch (IOException e){
                e.printStackTrace();
            }
        }

    }

    private void splitAndAssign(String currentLine){
        newSplit(currentLine, 5);
        nodes[Integer.parseInt(field[0])].edgesToNeighbors.
                add(new EdgeToNeighbor(Integer.parseInt(field[1]),Integer.parseInt(field[2])));
    }

    private void splitAndAssign2(String currentLine){
        //splitting the line and assigning all elements from split to "field"-attribute
        newSplit(currentLine, 5);

        //add the edgeToNeighbor to the nodes arrayList of neighbors
        invertedNodes[Integer.parseInt(field[1])].edgesToNeighbors.
                add(new EdgeToNeighbor(Integer.parseInt(field[0]),Integer.parseInt(field[2])));
    }



    /**
     * METHOD IS IN USE, HOWEVER IT IS COMMENTED OUT IN MAIN DUE TO HEAP - MEMORY ISSUES
     *
     * For inverting the node-weights in a graph
     * For use on pre-processing TO a landmark
     * File format: fromNode toNode time length speedLimit
     *
     * @param file is the file containing the weights
     */
    public void getInvertedWeightsFromFile( File file){


        BufferedReader bufferedReader = null;

        try{
            bufferedReader = new BufferedReader(new InputStreamReader(new FileInputStream(file)));

            //Doing this just because the first line is not to used in newSplit()
            String currentLine = bufferedReader.readLine();
            currentLine = currentLine.replace(" ", "");
            int lines = 0;

            while ((currentLine = bufferedReader.readLine()) != null) {
                if(lines == 100000){
                    System.gc();
                }
                splitAndAssign2(currentLine);
                currentLine = null;
                lines ++;
            }
        }catch (IOException e){
            e.printStackTrace();
        }
        /**finally {
            try{
                if (bufferedReader != null){
                    bufferedReader.close();
                }
            }catch (IOException e){
                e.printStackTrace();
            }
        }*/
    }

    /**
     * Description of method content:
             * field[0] = nodeNr (a nodes unique ID)
             * field[1] = nodeCode (# 1-4 describing type of node ex. gas station or hotel)
     * Fields have to be parsed to int because they are String-objects when read in from file
     *
     * @param file is the file containing nodeCode (format: nodeNr nodeCode nameOfPlace)
     */
    public void getNodeCodeAndNameFromFile(File file) {
        BufferedReader bufferedReader = null;

        try{
            bufferedReader = new BufferedReader(new FileReader(file));
            //Doing this just because the first line is not to used in newSplit()
            String currentLine = bufferedReader.readLine();
            while ((currentLine = bufferedReader.readLine()) != null) {

                //splitting the line and assigning all elements from split to "field"-attribute
                newSplit(currentLine, 3);

                //get the nodeNr and use it as an index
                int index = Integer.parseInt(field[0]);
                //get the node from adjacency list with the index
                Node currentNode = nodes[index];

                //assign the nodeCode and name to said node
                nodes[index].setNodeCode(Integer.parseInt(field[1]));
                nodes[index].setName(field[2]);
                //currentNode.setNodeCode(Integer.parseInt(field[1]));
                //currentNode.setName(field[2]);
                //currentNode.nodeCode = Integer.parseInt(field[1]);
                //currentNode.name = field[2];
            }
        }catch (IOException e){
            e.printStackTrace();
        }finally {
            try{
                if(bufferedReader != null){
                    bufferedReader.close();
                }
            }catch (IOException e){
                e.printStackTrace();
            }
        }

    }

    /**
     * Used twice, one time for each pre-processing table (To/From landmarks)
     *
     * @param file is the file to alter
     */
    public void writePreProcessingToFile(File file, int [][] preProcessedLandmarks){
        BufferedWriter bufferedWriter = null;
        try {
            bufferedWriter = new BufferedWriter(new FileWriter(file));
            for (int i = 0; i < preProcessedLandmarks.length; i++) {
                for (int j = 0; j < preProcessedLandmarks[i].length; j++) {
                    bufferedWriter.write(preProcessedLandmarks[i][j] + " ");
                }
                // new line for each node
                bufferedWriter.newLine();

            } bufferedWriter.flush();

        } catch (IOException e) {
            e.printStackTrace();
        }finally {
            try {
                if (bufferedWriter != null){
                    bufferedWriter.close();
                }
            }catch (IOException e){
                e.printStackTrace();
            }
        }
    }


    /**
     * Setting all distances to start equal to infinity for all nodes in graph
     * StartNode.distance = 0;
     */
    public void setDistances(Node[] graph){
        for (int i = 0; i < graph.length; i++) {
            graph[i].distanceToStart = infinity;
        }
        //startNode.distanceToStart = 0;
    }

    public void getCoordinates(ArrayList<Node> shortestPath){
        for (Node node : shortestPath){
            System.out.println(node.latitude + " , " + node.longitude);
        }
    }




    /**
     * To be able to print out array "nodes"
     * @return node w/array in certain position
     */
    @Override
    public String toString() {
        return "Nodes = " + Arrays.toString(nodes) +
                '\n';
    }
}
