import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.PriorityQueue;

/**
 * ALT Algorithm uses distance from landmarks to estimate distance to goal
 */
public class AltAlgorithm extends SuperClass {
    ArrayList<Node> shortestPath = new ArrayList<>(1000);
    ArrayList<Node> landMarks = new ArrayList<>(5); //does not need space for more than 5 nodes
    //int[][] distancesToLandmarks;
    //int[][] distancesFromLandmarks;

    public void readAllDistancesFromLandmarks(File fromLandmarks, int[][] preProcessingFromLandmarks, int numberOfVertices, ArrayList<Node> landMarks) {
        //preProcessingFromLandmarks = new int[numberOfVertices][landMarks.size()];
        BufferedReader bufferedReader = null;
        try {
            //FileInputStream fileInputStream= new FileInputStream(fromLandmarks);
            FileReader fileReader = new FileReader(fromLandmarks);
            //bufferedReader = new BufferedReader(new InputStreamReader(fileInputStream));
            bufferedReader = new BufferedReader(fileReader);

            String currentLine;
            int node = 0;
            int landmark = 0;

            //currentLine holds a nodes distance to all 5 landmarks
            while ((currentLine = bufferedReader.readLine()) != null) {

                //splitting the line and assigning all elements from split to "field"-attribute
                newSplit(currentLine, 5);

                //node 0, landmark 1 is added to the matrix
                preProcessingFromLandmarks[node][landmark] = Integer.parseInt(field[0]);
                //same node, landmark 2
                landmark ++;
                preProcessingFromLandmarks[node][landmark] = Integer.parseInt(field[1]);
                //same node, landmark 3
                landmark ++;
                preProcessingFromLandmarks[node][landmark] = Integer.parseInt(field[2]);
                //same node, landmark 4
                landmark ++;
                preProcessingFromLandmarks[node][landmark] = Integer.parseInt(field[3]);
                //same node, landmark 5
                landmark ++;
                preProcessingFromLandmarks[node][landmark] = Integer.parseInt(field[4]);

                //reset landmark and increment node by one as we start reading the next line
                landmark = 0;
                node++;
            }

        }  catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (bufferedReader != null){
                    bufferedReader.close();
                }
            }catch (IOException e){
                e.printStackTrace();
            }
        }

    }

    /**
     * Reads distances from all nodes to all landmarks from file
     *
     * @param toLandmarks file distances are read in from
     */
    public void readAllDistancesToLandmarks(File toLandmarks, int[][] preProcessingToLandmarks, int numberOfVertices, ArrayList<Node> landMarks) {
        BufferedReader bufferedReader = null;
        try {
            FileInputStream fileInputStream= new FileInputStream(toLandmarks);
            bufferedReader = new BufferedReader(new InputStreamReader(fileInputStream));

            String currentLine;
            int node = 0;
            int landmark = 0;

            //currentLine holds a nodes distance to all 5 landmarks
            while ((currentLine = bufferedReader.readLine()) != null) {

                //splitting the line and assigning all elements from split to "field"-attribute
                newSplit(currentLine, 5);

                //node 0, landmark 1 is added to the matrix
                preProcessingToLandmarks[node][landmark] = Integer.parseInt(field[0]);
                //same node, landmark 2
                landmark ++;
                preProcessingToLandmarks[node][landmark] = Integer.parseInt(field[1]);
                //same node, landmark 3
                landmark ++;
                preProcessingToLandmarks[node][landmark] = Integer.parseInt(field[2]);
                //same node, landmark 4
                landmark ++;
                preProcessingToLandmarks[node][landmark] = Integer.parseInt(field[3]);
                //same node, landmark 5
                landmark ++;
                preProcessingToLandmarks[node][landmark] = Integer.parseInt(field[4]);

                //reset landmark and increment node by one as we start reading the next line
                landmark = 0;
                node++;
            }

        }  catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (bufferedReader != null){
                    bufferedReader.close();
                }
            }catch (IOException e){
                e.printStackTrace();
            }
        }
    }


    /**
     * For use in ALT algorithm
     */
    public int getDistanceFromLandmark(Node currentNode, Node landmark, ArrayList<Node> landMarks, int [][] preProcessingFromLandmarks){
        int indexOfNode = currentNode.nodeNr;
        int indexOfLandM = landMarks.indexOf(landmark);

        return preProcessingFromLandmarks[indexOfNode][indexOfLandM];

    }

    /**
     * For use in ALT algorithm
     */
    public int getDistanceToLandmark(Node currentNode, Node landmark, ArrayList<Node> landMarks, int [][] preProcessingToLandmarks){
        int indexOfNode = currentNode.nodeNr;
        int indexOfLandM = landMarks.indexOf(landmark);

        return preProcessingToLandmarks[indexOfNode][indexOfLandM];
    }


    /**
     * Used everytime ALT finds a new node to put into PQ
     * Method combines using landmarks before start, after goal and landmarks in-between to find the biggest estimate
     * The following inequalities must be true:
             * Landmark (L) behind start: dist(n,goal) >= dist(L,goal) - dist(L,n)
             * Landmark (L) after goal  : dist(n,goal) >= dist(n,L) - dist(goal,L)
     * To get the distance between two nodes n and m:
             * Number 1: The distance from the i-th landmark to the goal (m) (preprocessing file)
                     * then, subtract the distance from the i-th landmark to Node n
             * Number 2: The distance from the Node n to the i-th landmark
                     * then, subtract the distance from the goal (m) to the i-th landmark
             * The biggest of Number 1 and Number 2 is the best estimate using the i-th landmark
     * Do this for all landmarks; the biggest number overall is the estimate we will use
     *
     * If a node gets a new distance label, just re-prioritize it (put it into the PQ again)
             * Don't use this method again, because the estimate does not change
     */
    public int getBiggestEstimateBetweenNodeAndLandmark(ArrayList<Node> landMarks, Node currentNode, Node goal, int[][] preProcessingToLandmarks, int[][] preProcessingFromLandmarks){
        int biggestEstimate = 0;
        for (int i = 0; i < landMarks.size(); i++) {
            int number1 = getDistanceFromLandmark(goal,landMarks.get(i),landMarks,preProcessingFromLandmarks) - getDistanceFromLandmark(currentNode,landMarks.get(i),landMarks,preProcessingFromLandmarks);
            int number2 = getDistanceToLandmark(currentNode,landMarks.get(i),landMarks,preProcessingToLandmarks) - getDistanceToLandmark(goal,landMarks.get(i),landMarks,preProcessingToLandmarks);

            if(number1 < 0){
                number1 = 0;
            }
            if (number1 > biggestEstimate){
                biggestEstimate = number1;
            }
            if (number2 > biggestEstimate){
                biggestEstimate = number2;
            }
        }
        return biggestEstimate;
    }

    /**
     * If endNode is taken out of the PQ, the search is over
     * Before this method:
         * Read in nodes and weights
         * Choose/set landmarks
         * Pre-process landmarks
     * Attribute "graph" holds the nodes
     *
     * @param startNode is the node the search starts in
     * @param endNode is the place we want to find the shortest path to
     */
    public void altAlgorithm(Node[] graph, ArrayList<Node> landMarks, Node startNode, Node endNode,int[][] preProcessingToLandmarks, int[][]preProcessingFromLandmarks){

        priorityQueue = new PriorityQueue<>(graph.length,new LandmarkComparator());
        notReached(graph);
        setDistances(graph);
        setStartNode(graph, startNode);
        setEndNode(endNode);
        priorityQueue.add(startNode);
        startNode.reached = true;
        int removedNodes = 0;

        //for timetaking
        long start = System.nanoTime();

        do {
            Node currentNode = priorityQueue.remove();
            for (EdgeToNeighbor edge : currentNode.edgesToNeighbors){
                // If statement skips neighbor if settled
                if(graph[edge.nodeNr].settled){
                    continue;
                }

                checkNeighbor(graph,landMarks,endNode, currentNode, edge, preProcessingToLandmarks, preProcessingFromLandmarks);
            }
            // Now, all the neighbors should be checked and either skipped (due to being settled) or put into pq
            // We are finished with the current node, so we settle it and continue
            currentNode.settled = true;
            removedNodes ++;
        }
        // We keep pulling nodes out of PQ until endNode is reached :-)
        while (!endNode.settled);

        //for time taking
        long end = System.nanoTime();
        long total = end-start;
        System.out.println("Alt algorithm time in seconds: " + (double)total/1000000000 +"s");

        //for stats
        System.out.println("removed nodes using alt algorithm: " + removedNodes);


    }

    /**
     * Support method for use inside of ALT algorithm
     * Adds node to PQ if it sees it fit
     *
     * @param nodeTakenFromPQ is the current node taken out of PQ inside ALT algorithm while-loop
     * @param edgeToNeighbor is the edge currently taken out of the ALT algorithm for-loop
     */
    public void checkNeighbor(Node[] graph, ArrayList<Node> landMarks, Node endNode, Node nodeTakenFromPQ, EdgeToNeighbor edgeToNeighbor, int[][] preProcessingToLandmarks, int[][]preProcessingFromLandmarks){
        // Retrieving the neighbor connected to the Node through the edge
        Node neighboringNode = graph[edgeToNeighbor.nodeNr];

        // If a neighboring node is reached, its distance to start will not be infinity, meaning this will work
        if(neighboringNode.reached){
            if (nodeTakenFromPQ.distanceToStart + edgeToNeighbor.time
                    < neighboringNode.distanceToStart){

                // nodeTakenFromPQ is now neighboringNode's parent
                neighboringNode.parent = nodeTakenFromPQ;

                // Neighboring node distance to start is updated!
                priorityQueue.remove(neighboringNode);
                neighboringNode.distanceToStart = nodeTakenFromPQ.distanceToStart + edgeToNeighbor.time;
                neighboringNode.distanceToGoal = getBiggestEstimateBetweenNodeAndLandmark(landMarks,neighboringNode,endNode,preProcessingToLandmarks,preProcessingFromLandmarks);
                neighboringNode.summedDistance = neighboringNode.distanceToStart + neighboringNode.distanceToGoal;

                // Nodes that are reached are put into the PQ
                priorityQueue.add(neighboringNode);
            }
        }
        // We end up here if the neighboring node was not reached (the difference is in the 2nd part of the condition)
        else {
            if (nodeTakenFromPQ.distanceToStart + edgeToNeighbor.time
                    < neighboringNode.distanceToStart){

                // nodeTakenFromPQ is now neighboringNode's parent
                neighboringNode.parent = nodeTakenFromPQ;

                // Neighboring node now has a distance to start
                neighboringNode.distanceToStart = nodeTakenFromPQ.distanceToStart + edgeToNeighbor.time;
                neighboringNode.summedDistance = neighboringNode.distanceToStart + neighboringNode.distanceToGoal;

                // - and is now reached
                neighboringNode.reached = true;

                // Nodes that are reached are put into the PQ
                priorityQueue.add(neighboringNode);
            }
        }
    }


    /**
     * This method must be used before pre-processing, but after loading in nodes, nodeCodes and names of nodes
     * Meaning methods getNodeAndLocation() getNodeCodeAndNameFromFile() and getWeightsFromFile() must be run first
     *
     * @param graph is implemented because the algorithm must now the specific node array we are working on
     *              (due to superclass and object instantiation all over during testing)
     * @param nodeNr1 landmark
     * @param nodeNr2 landmark
     * @param nodeNr3 landmark
     * @param nodeNr4 landmark
     * @param nodeNr5 landmark
     *
     * @throws Exception if the methods named above are not run yet
     */
    public void setLandmarksUsingNodeNr(Node[] graph, ArrayList<Node> landMarks, int nodeNr1, int nodeNr2, int nodeNr3, int nodeNr4, int nodeNr5) throws Exception {
        if(graph.length == 0){
            throw new Exception("The nodes must be loaded in from File before landmarks can be chosen");
        }
        for (Node node : graph) {
            if (node.nodeNr == nodeNr1 || node.nodeNr == nodeNr2 || node.nodeNr == nodeNr3
                    || node.nodeNr == nodeNr4 || node.nodeNr == nodeNr5) {
                landMarks.add(node);
            }
        }
        if(landMarks.size() != 5){
            throw new IllegalArgumentException("Only " + landMarks.size() + " landmarks were found. " +
                    "Did you spell the names correctly?");
        }
    }


    /**
     * First:
         * Read in nodes
         * Read in weights (and by default, neighbors)
         * Assign landmarks
     *
     * Method works by looping through the landmarks inside the landmarks array,
     * meaning that the landmarks have to be assigned before this can be done
     * SuperClass.Dijkstra's algorithm searches through all nodes in nodes array using each landmark as startingNode
     *
     * Method fills matrix "fromLandmarks" with
         * array #1 holds the node-indexes
         * array #2 holds the landmark-indexes
     */
    public void fillFromLandmarksMatrix(Dijkstra dijkstra, Node[] graph, ArrayList<Node> landMarks, int[][] preProcessedLandmarks) {

        for (Node landmark : landMarks) {
            // Fills up the table holding pre-processed landmarks using the landmark as starting node
            dijkstra.dijkstraAlgorithm(graph, landmark, landMarks, preProcessedLandmarks);
        }

    }

    /**
     * Before this can be done, the method getInvertedWeightsFromFile() must be used to reverse/invert the graph
     *
     * @param dijkstra
     */
    public void fillToLandmarksMatrix(Dijkstra dijkstra, Node[] graph, ArrayList<Node> landMarks, int[][] preProcessedLandmarks){
        for(Node landmark: landMarks){
            // Running Dijkstra's algorithm fills up the table holding pre-processed landmarks
            dijkstra.dijkstraAlgorithm(graph,landmark, landMarks, preProcessedLandmarks);
        }
    }

    /**
     * Cleans out the matrix (2D) holding the preprocessing information
     */
    public void cleanOutPreProcessingMatrix(int[][] preProcessedLandmarks) {
        Arrays.stream(preProcessedLandmarks).forEach(x -> Arrays.fill(x, 0));
    }

    /**
     * Method gets the shortest path from endNode to startNode by getting parents recursively
     * Base case is when currentNode = startNode
     *
     * @param currentNode must be set to endNode
     */
    public void getShortestPath(Node currentNode){
        //"base case"
        if(currentNode != startNode){
            shortestPath.add(currentNode);
            currentNode = currentNode.parent;
            getShortestPath(currentNode);
        }
    }

    public static void main(String[] args)  {
        // The files needed for testing
        File islandNodeAndLocation = new File("NodeInformation/IslandNodes.txt");
        File islandWeights = new File("NodeInformation/IslandWeights.txt");
        File islandNodeCodeAndName = new File("NodeInformation/IslandCodeAndName.txt");

        // Read in everything
        SuperClass superClass = new SuperClass();
        superClass.getNodeAndLocationFromFile(islandNodeAndLocation);
        superClass.getWeightsFromFile(islandWeights);
        superClass.getNodeCodeAndNameFromFile(islandNodeCodeAndName);


        // Set landmarks
        AltAlgorithm altAlgorithm = new AltAlgorithm();
        try {
            altAlgorithm.setLandmarksUsingNodeNr(superClass.nodes, superClass.landMarks, 10,100,3600,3000,10700);
        } catch (Exception e){
            e.printStackTrace();
        }


        // Dijkstra object needed to run Dijkstra algorithm
        Dijkstra dijkstra = new Dijkstra();


        // Preprocessing from the landmarks
        File fromLandmarks = new File("Landmarks/FromLandmark/from");
        altAlgorithm.fillFromLandmarksMatrix(dijkstra, superClass.nodes, superClass.landMarks, superClass.preProcessingFromLandmarks);
        altAlgorithm.writePreProcessingToFile(fromLandmarks, superClass.preProcessingFromLandmarks);

        System.out.println("Preprocessing of 'from landmarks' is complete with " + superClass.preProcessingFromLandmarks.length + "objects\n");

        // Because the preprocessing from and to is done on the same 2D matrix, it must be emptied now
        altAlgorithm.cleanOutPreProcessingMatrix(superClass.preProcessingFromLandmarks);

        System.out.println("preprocessing - matrix is cleaned out\n");

        // Preprocessing to the landmarks
        File toLandmarks = new File("Landmarks/ToLandmark/to");
        altAlgorithm.fillToLandmarksMatrix(dijkstra, superClass.invertedNodes, superClass.landMarks, superClass.preProcessingToLandmarks);
        altAlgorithm.writePreProcessingToFile(toLandmarks, superClass.preProcessingToLandmarks);

        System.out.println("Preprocessing of 'to landmarks' is complete with " + superClass.preProcessingToLandmarks.length + "objects\n");

        // Read in preprocessed information for use in ALT
        altAlgorithm.readAllDistancesFromLandmarks(fromLandmarks, superClass.preProcessingFromLandmarks, superClass.numberOfVertices, superClass.landMarks);
        altAlgorithm.readAllDistancesToLandmarks(toLandmarks, superClass.preProcessingToLandmarks, superClass.numberOfVertices, superClass.landMarks);

        //alt :-)
        Node startNode = superClass.nodes[14207];
        Node endNode = superClass.nodes[64365];
        altAlgorithm.altAlgorithm(superClass.nodes, superClass.landMarks, startNode,endNode, superClass.preProcessingToLandmarks,superClass.preProcessingFromLandmarks);

        System.out.println(endNode);
        //recursive iteration from end to start to get the shortest path through the nodes' parents
        altAlgorithm.getShortestPath(endNode);
        System.out.println("Size of shortest path: " + altAlgorithm.shortestPath.size());





        //get the time
        int endTime = endNode.distanceToStart/100;
        System.out.print("Total time by driving (Dijkstra): " + endTime/3600+"h ");
        System.out.print((endTime%3600)/60+"min ");
        System.out.print((endTime%3600)%60+"sec\n");
    }
}
