import java.io.File;
import java.util.Arrays;

public class Main {

    public static void main(String[] args) {
        SuperClass superClass = new SuperClass();
        Dijkstra dijkstra = new Dijkstra();
        AltAlgorithm altAlgorithm = new AltAlgorithm();


        //reading nodes in from file, is length of graph the same as numberOfVertices
        File nodeAndLocation = new File("NodeInformation/nodesAndLocation.txt");
        superClass.getNodeAndLocationFromFile(nodeAndLocation);


        //get the names and codes from file
        File codeAndName = new File("NodeInformation/CodeAndName.txt");
        superClass.getNodeCodeAndNameFromFile(codeAndName);

        //get the weights
        File weights = new File("NodeInformation/weights.txt");

        /**
        //copy nodes onto inverted nodes
        superClass.copyNodesListOntoInvertedNodes(superClass.numberOfVertices, superClass.nodes);
        superClass.getInvertedWeightsFromFile(weights);

         */

        //landmarks file
        File fromLandmarks = new File("Landmarks/FromLandmark/from");
        File toLandmarks = new File("Landmarks/ToLandmark/to");


        /**
        // Set landmarks
        try {
            //node1=torvikbukt, node2= "OKQ8", node3 = søre osen, node4 = kvitnes, node5 = extra tingvoll
            altAlgorithm.setLandmarksUsingNodeNr(superClass.nodes, superClass.landMarks, 5392124,3349203,5425100,5683930,2601777);
        } catch (Exception e){
            e.printStackTrace();
        }
         */

        //set the landmarks on a bigger scale!
        try {
            //node1 = St1 Kangasala, node2= "Hiirola", node3 = Circle K Automat Båtsfjord, node4 = Ope, node5 = YX Truck Veøy Kristiansand
            altAlgorithm.setLandmarksUsingNodeNr(superClass.nodes, superClass.landMarks, 5978929,6410530,5793346,3430252,2313275	);
        } catch (Exception e){
            e.printStackTrace();
        }

        /**

        // Preprocessing to the landmarks
        altAlgorithm.fillToLandmarksMatrix(dijkstra, superClass.invertedNodes, superClass.landMarks, superClass.preProcessingToLandmarks);
        altAlgorithm.writePreProcessingToFile(toLandmarks, superClass.preProcessingToLandmarks);

        System.out.println("Preprocessing of 'to landmarks' is complete with " + superClass.preProcessingToLandmarks.length + "objects\n");

        //clean out the array holding on to the invertedNodes
        Arrays.fill(superClass.invertedNodes, null);

        // Because the preprocessing from and to is done on the same 2D matrix, it must be emptied now
        altAlgorithm.cleanOutPreProcessingMatrix(superClass.preProcessingToLandmarks);
        */

        //after inverted nodes are preprocessed
        superClass.getWeightsFromFile(weights);


        /**
        // Preprocessing from the landmarks
        altAlgorithm.fillFromLandmarksMatrix(dijkstra, superClass.nodes, superClass.landMarks, superClass.preProcessingFromLandmarks);
        altAlgorithm.writePreProcessingToFile(fromLandmarks, superClass.preProcessingFromLandmarks);

        System.out.println("Preprocessing of 'from landmarks' is complete with " + superClass.preProcessingFromLandmarks.length + "objects\n");

        altAlgorithm.cleanOutPreProcessingMatrix(superClass.preProcessingFromLandmarks);
         */

        // Read in preprocessed information for use in ALT
        altAlgorithm.readAllDistancesFromLandmarks(fromLandmarks, superClass.preProcessingFromLandmarks, superClass.numberOfVertices, superClass.landMarks);
        altAlgorithm.readAllDistancesToLandmarks(toLandmarks,superClass.preProcessingToLandmarks, superClass.numberOfVertices, superClass.landMarks);

        /**
        //data for route Kårvåg–Gjemnes
        SuperClass.Node kårvåg = superClass.nodes[6368906];
        SuperClass.Node gjemnes = superClass.nodes[6789998];

        //alt :-)
        SuperClass.Node startNode = kårvåg;
        SuperClass.Node endNode = gjemnes;
        altAlgorithm.altAlgorithm(superClass.nodes, superClass.landMarks, startNode,endNode,superClass.preProcessingToLandmarks,superClass.preProcessingFromLandmarks);

        System.out.println(endNode);
        //recursive iteration from end to start to get the shortest path through the nodes' parents
        altAlgorithm.getShortestPath(endNode);
        System.out.println("Size of shortest path: " + altAlgorithm.shortestPath.size());

        superClass.getCoordinates(altAlgorithm.shortestPath);

        System.out.println(endNode);


        //get the time
        int endTime = endNode.distanceToStart/100;
        System.out.print("Total time by driving (ALT): " + endTime/3600+"h ");
        System.out.print((endTime%3600)/60+"min ");
        System.out.print((endTime%3600)%60+"sec\n");
         */




        //choose tampere as startnode
        SuperClass.Node tampere = superClass.nodes[136963];
        //choose trondheim as endNode
        SuperClass.Node trondheim = superClass.nodes[6861306];

        //empty out the shortestPath array
        altAlgorithm.shortestPath.clear();

        //alt between the two nodes
        altAlgorithm.altAlgorithm(superClass.nodes, superClass.landMarks, tampere,trondheim, superClass.preProcessingToLandmarks,superClass.preProcessingFromLandmarks);

        //get the shortest path
        altAlgorithm.getShortestPath(trondheim);

        //get locations
        superClass.getCoordinates(altAlgorithm.shortestPath);

        //print of the endNode
        System.out.println("Trondheim: " + trondheim + "\n");

        //get the time
        int endTime3 = trondheim.distanceToStart/100;
        System.out.print("Total time by driving (ALT): " + endTime3/3600+"h ");
        System.out.print((endTime3%3600)/60+"min ");
        System.out.print((endTime3%3600)%60+"sec\n");


        /**
        //reading in weights
        File weights = new File("NodeInformation/weights.txt");
        superClass.getWeightsFromFile(weights);




        //choose the airport as startNode
        SuperClass.Node værnes = superClass.nodes[6590451];

        //choose the hotell as startNode
        SuperClass.Node rørosHotell = superClass.nodes[1419364	];

        //use dijkstra's algorithm to find 10 gas stations near the airport
        dijkstra.dijkstraAlgorithmNodeCode(superClass.nodes, værnes, 2, 10);

        //get the coordinates for testing that the gas stations were actually gas stations
        dijkstra.getCoordinates(dijkstra.shortestPath);


        //empty out shortestPath array
        dijkstra.shortestPath.clear();


        //use dijkstra's algorithm to find 10 charging stations near the hotell
        dijkstra.dijkstraAlgorithmNodeCode(superClass.nodes, rørosHotell, 4,10);

        //get the coordinates for testing that the gas stations were actually gas stations
        dijkstra.getCoordinates(dijkstra.shortestPath);

        //shortest path is now inside of attribute shortestPath (ArrayList)
        System.out.println("Length of shortest path: " + dijkstra.shortestPath.size());

        //clear out the array again
        dijkstra.shortestPath.clear();

        //data for route Kårvåg–Gjemnes
        SuperClass.Node kårvåg = superClass.nodes[6368906];
        SuperClass.Node gjemnes = superClass.nodes[6789998];
        dijkstra.dijkstraAlgorithm(superClass.nodes,kårvåg,gjemnes);

        //få tak i tiden tatt
        System.out.println(gjemnes);

        //the route is inside of shortest path
        dijkstra.getShortestPath(gjemnes);
        System.out.println("Length of shortest path: " + dijkstra.shortestPath.size());
        dijkstra.getCoordinates(dijkstra.shortestPath);


        // check that inverted nodes are of ok size
        System.out.println("size of inverted nodes" + superClass.invertedNodes.length);
        System.out.println("node 1 from inverted nodes" + superClass.invertedNodes[0]);

        // Set landmarks
        AltAlgorithm altAlgorithm = new AltAlgorithm();
        try {
            altAlgorithm.setLandmarksUsingNodeNr(superClass.nodes, superClass.landMarks, 10,100,3600,3000,10700);
        } catch (Exception e){
            e.printStackTrace();
        }

        // Get the inverted weights for the second half of preprocessing
        superClass.getInvertedWeightsFromFile(weights);

        // Preprocessing from the landmarks
        File fromLandmarks = new File("Landmarks/FromLandmark/from");
        altAlgorithm.fillFromLandmarksMatrix(dijkstra, superClass.nodes, superClass.landMarks, superClass.preProcessingFromLandmarks);
        altAlgorithm.writePreProcessingToFile(fromLandmarks, superClass.preProcessingFromLandmarks);

        System.out.println("Preprocessing of 'from landmarks' is complete with " + superClass.preProcessingFromLandmarks.length + "objects\n");

        // Because the preprocessing from and to is done on the same 2D matrix, it must be emptied now
        altAlgorithm.cleanOutPreProcessingMatrix(superClass.preProcessingFromLandmarks);

        System.out.println("preprocessing - matrix is cleaned out\n");

        // Preprocessing to the landmarks
        File toLandmarks = new File("Landmarks/ToLandmark/to");
        altAlgorithm.fillToLandmarksMatrix(dijkstra, superClass.invertedNodes, superClass.landMarks, superClass.preProcessingToLandmarks);
        altAlgorithm.writePreProcessingToFile(toLandmarks, superClass.preProcessingToLandmarks);

        System.out.println("Preprocessing of 'to landmarks' is complete with " + superClass.preProcessingToLandmarks.length + "objects\n");

        // Read in preprocessed information for use in ALT
        altAlgorithm.readAllDistancesFromLandmarks(fromLandmarks, superClass.numberOfVertices, superClass.landMarks);
        altAlgorithm.readAllDistancesToLandmarks(toLandmarks, superClass.numberOfVertices, superClass.landMarks);

        //alt :-)
        SuperClass.Node startNode = kårvåg;
        SuperClass.Node endNode = gjemnes;
        altAlgorithm.altAlgorithm(superClass.nodes, superClass.landMarks, startNode,endNode,toLandmarks,fromLandmarks);

        System.out.println(endNode);
        //recursive iteration from end to start to get the shortest path through the nodes' parents
        altAlgorithm.getShortestPath(endNode, startNode);
        System.out.println("Size of shortest path: " + altAlgorithm.shortestPath.size());

        */


    }
}
