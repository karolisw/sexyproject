import java.util.Comparator;

public class LandmarkComparator implements Comparator<SuperClass.Node> {

    /**
     * This comparator is for use in ALT algorithm's priority queue
     * The comparator compares uses the sum of the nodes distance from start and the distance to the goal
     * Meaning, it uses node.summedDistance to compare
     */
    public int compare(SuperClass.Node node1, SuperClass.Node node2) {
        return node1.summedDistance - node2.summedDistance;
        /**
        if (node1.summedDistance > node2.summedDistance) {
            return 1;
        } else if (node1.summedDistance < node2.summedDistance) {
            return -1;
        }
        return 0;
         */
    }
}
