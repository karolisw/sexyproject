import java.util.Comparator;

public class DistanceToStartComparator implements Comparator<SuperClass.Node> {

    /**
     * Overriding compare method in order to compare nodes by distance
     */
    public int compare(SuperClass.Node node1, SuperClass.Node node2) {
        if (node1.distanceToStart > node2.distanceToStart) {
            return 1;
        } else if (node1.distanceToStart < node2.distanceToStart) {
            return -1;
        }
        return 0;
    }
}
