import java.io.File;
import java.util.ArrayList;
import java.util.PriorityQueue;

/**
 * SuperClass.Dijkstra's algorithm uses the distance from startNode as priority when choosing the node to take out
 * of PQ
 */
public class Dijkstra extends SuperClass {
    ArrayList<Node> shortestPath = new ArrayList<>(1000);
    /**
     * For use in preProcessing as it does not end until pq is empty
     *
     * @param graph all nodes (insert "nodes" array from SuperClass as argument)
     * @param startNode the landmark in question
     */
    public void dijkstraAlgorithm(Node[] graph, Node startNode, ArrayList<Node> landMarks, int[][] preProcessedLandmarks){
        priorityQueue = new PriorityQueue<>(graph.length,new DistanceToStartComparator());
        notReached(graph);
        setDistances(graph);
        setStartNode(graph, startNode);
        priorityQueue.add(startNode);
        startNode.reached = true;

        do {
            Node currentNode = priorityQueue.remove();
            for (EdgeToNeighbor edge : currentNode.edgesToNeighbors){
                // If statement skips neighbor if settled
                if(graph[edge.nodeNr].settled){
                    continue;
                }
                checkNeighbor(currentNode, edge, graph);
            }
            // Now, all the neighbors should be checked and either skipped (due to being settled) or put into pq
            // We are finished with the current node, so we settle it and continue
            currentNode.settled = true;

            // A settled node is taken out of PQ, and has a set distance
            // Therefore, it is ready to be put into the matrix to/from landmarks
            // Format of this operation :[nodeNr][landmarks.getIndex(startNode.nodeNr)] = node.distanceToStart
            preProcessedLandmarks [currentNode.nodeNr][landMarks.indexOf(startNode)] = currentNode.distanceToStart;
        }
        // We keep pulling nodes out of PQ until all nodes are removed :-)
        while (priorityQueue.size() > 0);
        System.out.println("Finished preprocessing landmark: " + landMarks.indexOf(startNode));
    }

    /**
     * Not for use in preprocessing, but as a self-standing algorithm
     *
     * @param graph is the node array with the nodes and weights loaded in from file
     * @param startNode is chosen manually (the whole object)
     * @param endNode is chosen manually (the whole object)
     */
    public void dijkstraAlgorithm(Node[] graph, Node startNode, Node endNode){


        priorityQueue = new PriorityQueue<>(graph.length,new DistanceToStartComparator());
        notReached(graph);
        setDistances(graph);
        setStartNode(graph, startNode);
        setEndNode(endNode);
        priorityQueue.add(startNode);
        startNode.reached = true;
        int removedNodes = 0;

        //for timetaking
        long start = System.nanoTime();

        do {
            Node currentNode = priorityQueue.remove();
            for (EdgeToNeighbor edge : currentNode.edgesToNeighbors){
                // If statement skips neighbor if settled
                if(graph[edge.nodeNr].settled){
                    continue;
                }
                checkNeighbor(currentNode, edge, graph);
            }
            // Now, all the neighbors should be checked and either skipped (due to being settled) or put into pq
            // We are finished with the current node, so we settle it and continue
            currentNode.settled = true;
            removedNodes ++;
        }
        // We keep pulling nodes out of PQ until endNode is settled :-)
        while (!endNode.settled);

        //for time taking
        long end = System.nanoTime();
        long total = end-start;
        System.out.println("Dijkstra's algorithm time in seconds: " + (double)total/1000000000 +"s");

        System.out.println("Removed nodes using Dijkstra's algorithm: " + removedNodes);
        System.out.println("Total amount of nodes: " + graph.length + "\n");
    }

    public void checkNeighbor(Node currentNode, EdgeToNeighbor edge, Node[] graph){
        // Find the node that the edge connects the currentNode to (find the neighboring node)
        Node neighbor = graph[edge.nodeNr];
        // The neighbor will have MAX_INT as distanceToStart unless it is previously reached
        if (currentNode.distanceToStart + edge.time < neighbor.distanceToStart){
            // If true, the neighbor receives a new distance, and is reached
            neighbor.distanceToStart = currentNode.distanceToStart + edge.time;
            neighbor.reached = true;
            //The parent of the newly reached neighbor is currentNode at this point :-)
            neighbor.parent = currentNode;
            // A newly reached node should be put into PQ
            priorityQueue.add(neighbor);
        }
    }

    /**
     * For use in finding a desired amount of place-type
     * Ex. finding 10 gas stations
     * NodeCode 1 = city/place
     * NodeCode 2 = gas station
     * NodeCode 3 = charging station
     *
     * @param graph is the node array with the nodes and weights loaded in from file
     * @param startNode is chosen manually (the whole object)
     */
    public void dijkstraAlgorithmNodeCode(Node[] graph, Node startNode, int nodeCode, int numberOfDesiredCodes){
        priorityQueue = new PriorityQueue<>(graph.length,new DistanceToStartComparator());
        notReached(graph);
        setDistances(graph);
        setStartNode(graph, startNode);
        priorityQueue.add(startNode);
        startNode.reached = true;
        int removedNodes = 0;
        int addedNodes = 0;

        do {
            // add the node to the list
            Node currentNode = priorityQueue.remove();
            if (currentNode.nodeCode == nodeCode){
                shortestPath.add(currentNode);
                addedNodes ++;
            }
            for (EdgeToNeighbor edge : currentNode.edgesToNeighbors){
                // If statement skips neighbor if settled
                if(graph[edge.nodeNr].settled){
                    continue;
                }
                checkNeighbor(currentNode, edge, graph);
            }
            // Now, all the neighbors should be checked and either skipped (due to being settled) or put into pq
            // We are finished with the current node, so we settle it and continue
            currentNode.settled = true;
            removedNodes ++;
        }
        // We keep pulling nodes out of PQ until endNode is settled :-)
        while (addedNodes != numberOfDesiredCodes);
        System.out.println("Removed nodes using Dijkstra's algorithm: " + removedNodes);
        System.out.println("Total amount of nodes: " + graph.length + "\n");
    }


    /**
     * Method gets the shortest path from endNode to startNode by getting parents recursively
     * Base case is when currentNode = startNode
     *
     * @param currentNode must be set to endNode
     */
    public void getShortestPath(Node currentNode){
        //"base case"
        if(currentNode != startNode){
            shortestPath.add(currentNode);
            currentNode = currentNode.parent;
            getShortestPath(currentNode);
        }
    }


    public void getCoordinates(ArrayList<Node> shortestPath){
        super.getCoordinates(shortestPath);

    }


    public static void main(String[] args) {
        SuperClass superClass = new SuperClass();
        Dijkstra dijkstra = new Dijkstra();

        //reading nodes in from file, is length of graph the same as numberOfVertices
        File nodeAndLocation = new File("NodeInformation/nodesAndLocation.txt");
        superClass.getNodeAndLocationFromFile(nodeAndLocation);

        //copy nodes onto inverted nodes
        superClass.copyNodesListOntoInvertedNodes(superClass.nodes.length, superClass.nodes);

        //reading in weights
        File weights = new File("NodeInformation/weights.txt");
        superClass.getWeightsFromFile(weights);

        //get the names and codes from file
        File codeAndName = new File("NodeInformation/CodeAndName.txt");
        superClass.getNodeCodeAndNameFromFile(codeAndName);

        //choose the airport as startNode
        Node værnes = superClass.nodes[6590451];

        //choose the hotell as startNode
        Node rørosHotell = superClass.nodes[1419364	];


        //use dijkstra's algorithm to find 10 gas stations near the airport
        dijkstra.dijkstraAlgorithmNodeCode(superClass.nodes, værnes, 2, 10);

        //get the coordinates for testing that the gas stations were actually gas stations
        dijkstra.getCoordinates(dijkstra.shortestPath);


        //empty out shortestPath array
        dijkstra.shortestPath.clear();


        //use dijkstra's algorithm to find 10 charging stations near the hotell
        dijkstra.dijkstraAlgorithmNodeCode(superClass.nodes, rørosHotell, 4,10);

        //get the coordinates for testing that the gas stations were actually gas stations
        dijkstra.getCoordinates(dijkstra.shortestPath);

        //shortest path is now inside of attribute shortestPath (ArrayList)
        System.out.println("Length of shortest path: " + dijkstra.shortestPath.size());

        //clear out the array again
        dijkstra.shortestPath.clear();

        //data for route Kårvåg–Gjemnes
        Node kårvåg = superClass.nodes[6368906];
        Node gjemnes = superClass.nodes[6789998];
        dijkstra.dijkstraAlgorithm(superClass.nodes,kårvåg,gjemnes);


        //the route is inside of shortest path
        dijkstra.getShortestPath(gjemnes);
        System.out.println("Length of shortest path: " + dijkstra.shortestPath.size());

        //get the time
        int endTime = gjemnes.distanceToStart/100;
        System.out.print("Total time by driving (Dijkstra): " + endTime/3600+"h ");
        System.out.print((endTime%3600)/60+"min ");
        System.out.print((endTime%3600)%60+"sec\n");

        //clear it so that it can be used underneath
        dijkstra.shortestPath.clear();

        //choose tampere as startnode
        Node tampere = superClass.nodes[136963];
        //choose trondheim as endNode
        Node trondheim = superClass.nodes[6861306];

        //finding the distance from tampere to trondheim
        dijkstra.dijkstraAlgorithm(superClass.nodes,tampere,trondheim);

        //get the shortest path
        dijkstra.getShortestPath(trondheim);

        //get the locations :-)
        superClass.getCoordinates(dijkstra.shortestPath);

        System.out.println("trondheim: " + trondheim);

        //get the time
        int endTime2 = trondheim.distanceToStart/100;
        System.out.print("Total time by driving (Dijkstra): " + endTime2/3600+"h ");
        System.out.print((endTime2%3600)/60+"min ");
        System.out.print((endTime2%3600)%60+"sec\n");

    }


}
