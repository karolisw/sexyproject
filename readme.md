###Landmarks directory 
The directory contains files with information about landmark preprocessing. 
The directory contains two new directories, one for holding a file containing a matrices 
with information from landmarks, and one that holds the matrix with information to landmarks. 
There are two files, meaning that: 
- one file holds a matrix with all 5 landmarks and their distances to all the nodes
- one file holds a matrix with all 5 landmarks and the nodes' distances to the landmarks

###NodeInformation directory 
The directory contains the files given by the exercise - text. The files are loaded into the computer locally. 
Each file is used to obtain the information needed to run the algorithms, and should either:
- Not be discarded of when turning in the exercise
- Or, the method for loading the files in needs to be included in the main method running the entire ting. This is because we will not be able to run the code without them

###About all the commented - out code in main
The code in main that is commented out is to illustrate what has been done up until now. 
Had the files not been uploaded along with the code, the code would have had to be run again.
The code that is commented out is in relation to pre-processing, and testing on the island map. 
There is also some code used to get the information for getting coordinates and stats needed to complete the task. 
Had it not been for the fact that we would get heap out of memory, we would have un-commented out the code